#pragma once

#include "../Constants/Constants.h"
#include "Component.h"
#include "EntityManager.h"
#include <vector>
#include <map>
#include <algorithm> 
#include <string>
#include <typeinfo>
#include <iostream>

class Component;
class Entitymanager;

class Entity {
private:
	EntityManager& manager;
	bool isActive;
	std::vector<Component*> components;
	std::map<const std::type_info*, Component*> allEntityComponents;
public:
	std::string name;
	LayerType layer;
	Entity(EntityManager& manager);
	Entity(EntityManager& manager, std::string name, LayerType layer);
	void  PrintAllComponentsAttached() const;
	void Update(float deltaTime);
	void Render();
	void Destroy();
	bool IsActive() const;



	//TEMPLATES
	template <typename T, typename... TArgs>T& AddComponent(TArgs&&... args) 
	{
		T* newComponent(new T(std::forward<TArgs>(args)...));

		newComponent->owner = this;

		components.emplace_back(newComponent);

		allEntityComponents[&typeid(*newComponent)] = newComponent;

		newComponent->Initialize();

		return *newComponent;
	}

	template <typename T> T* GetComponent()
	{ 
		return static_cast<T*> (allEntityComponents[&typeid(T)]); 
	}

	template <typename T> bool HasComponent() {
		auto hasComponent = static_cast<T*> (allEntityComponents[&typeid(T)]);

		if (hasComponent) return true;

		return false;
	}



	template <typename T, typename... TArgs>T& RemoveComponent(TArgs&&... args) //todo refactor here and use map
	{
		T* newComponent(new T(std::forward<TArgs>(args)...));
	//	auto hasComponent = std::find(components.begin(), components.end(), newComponent) != components.end();

		auto typeToDelete = typeid(newComponent).name();

		for (unsigned int i = 0; i < components.size(); i++) 
		{
			auto tryCast = dynamic_cast<T*>(components[i]);
			auto currentCheckType = typeid(tryCast).name();

			if (typeToDelete == currentCheckType) {
				components.erase(std::remove(components.begin(), components.end(), components[i]), 
					components.end());
			}

		}

		components.shrink_to_fit();
		return *newComponent;
	}
};

