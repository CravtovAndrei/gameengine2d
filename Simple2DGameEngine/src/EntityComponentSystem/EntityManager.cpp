#include "EntityManager.h"
#include "Entity.h"
#include "../Components/Colliders/ColliderComponent.h"
#include "../Engine/Collision/Collision.h"


void EntityManager::CleardData()
{
	for (auto& entity : entities) {
		entity->Destroy();
	}
}

void EntityManager::Update(float deltaTime)
{
	for (auto& entity : entities) {
		entity->Update(deltaTime);
	}
	DestroyDisabledEntities();
}

void EntityManager::DestroyDisabledEntities(){
	for(int i = 0; i < entities.size(); i++){
		if(!entities[i]->IsActive()){
			entities.erase(entities.begin() + i);
			//std::removeif
		}
	}
}

void EntityManager::Render()
{
	for (int layerNumber = 0; layerNumber < NUM_LAYERS; layerNumber++) 
	{
		for (auto& entity : GetEntitiesByLayer(static_cast<LayerType>(layerNumber))) 
		{
			entity->Render();
		}
	}
}

bool EntityManager::HasNoEntities()
{
	return entities.size() == 0;
}

Entity& EntityManager::AddEntity(std::string entityName, LayerType layer )
{
	Entity* entity = new Entity(*this, entityName, layer);
	entities.emplace_back(entity);
	return *entity;
}


std::vector<Entity*> EntityManager::GetEntities() const
{
	return entities;
}

std::vector<Entity*> EntityManager::GetEntitiesByLayer(LayerType layer) const
{
	std::vector<Entity*> selectedEntities;
	for (auto& entity : entities) {
		if (entity->layer == layer) {
			selectedEntities.emplace_back(entity);
		}
	}
	return selectedEntities;
}

CollisionType EntityManager::CheckEntityCollisions() const //TODO REFACTOR ALL THIS!
{
	for (auto& thisEntity : entities) 
	{
		if (thisEntity->HasComponent<ColliderComponent>()) 
		{
			ColliderComponent* thisCollider = thisEntity->GetComponent<ColliderComponent>();

			for (auto& thatEntity : entities)
			{
				if (thisEntity->name.compare(thatEntity->name) != 0 && thatEntity->HasComponent<ColliderComponent>()) 
				{
					ColliderComponent* thatCollider = thatEntity->GetComponent<ColliderComponent>();

					if (Collision::CheckRectangleCollision(thisCollider->collider, thatCollider->collider)) 
					{

						if (thisCollider->colliderTag.compare("PLAYER") == 0 && thatCollider->colliderTag.compare("ENEMY") == 0) {
							return PLAYER_ENEMY_COLLISION;}

						if (thisCollider->colliderTag.compare("PLAYER") == 0 && thatCollider->colliderTag.compare("PROJECTILE") == 0) {
							return PLAYER_PROJECTILE_COLLISION;}

						if (thisCollider->colliderTag.compare("PLAYER") == 0 && thatCollider->colliderTag.compare("FRIENDLY_PROJECTILE") == 0) {
							return FRIENDLY_PROJECTILE_COLLISION;}

						if (thisCollider->colliderTag.compare("PLAYER") == 0 && thatCollider->colliderTag.compare("LEVEL_COMPLETE") == 0) {
							return PLAYER_LEVEL_COMPLETE_COLLISION;}

					}
				}
			}
		}
	}
}


std::vector<std::string> EntityManager::ReturnAllEntitiesNames()
{
	std::vector<std::string> allEntitiesNames;

	for (auto entity : entities) {
		allEntitiesNames.push_back(entity->name);
	}

	return allEntitiesNames;
}

void EntityManager::PrintAllEntitiesToConsole() const
{
	for (auto entity : entities) {
		std::cout <<"Entity Manager contains: " << entity->name << std::endl;
	}
}

unsigned int EntityManager::GetEntityCount()
{
	return entities.size();
}
