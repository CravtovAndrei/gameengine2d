#pragma once

#include "Component.h"
#include <vector>
#include <string>
#include "../Constants/Constants.h"

class Entity;

class EntityManager {
private:
	std::vector<Entity*> entities;

public:
	void CleardData();
	void Update(float deltaTime);
	void DestroyDisabledEntities();
	void Render();
	bool HasNoEntities();
//	Entity& AddEntity(std::string entityName);
	Entity& AddEntity(std::string entityName, LayerType layer);
	std::vector<Entity*> GetEntities() const;
	std::vector<Entity*> GetEntitiesByLayer(LayerType layer) const;

	CollisionType CheckEntityCollisions() const;
	std::vector<std::string> ReturnAllEntitiesNames();
	void PrintAllEntitiesToConsole() const;
	unsigned int GetEntityCount();
};
