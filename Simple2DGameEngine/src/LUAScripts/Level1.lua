dofile("./src/Constants/LuaTypeConstants.lua")

--print(transformType)
--print(colliderType)

Level1 = {


	assets = {
		[0] = {type = "texture", id = "Bullet", file = "./assets/images/bullet.png" },
		[1] = {type = "texture", id = "ChopperSpriteSheet", file = "./assets/images/chopper-spritesheet.png" },
		[2] = {type = "texture", id = "TileMap", file = "./assets/tilemaps/jungle.png" },
		[3] = {type = "font", id = "charriot-font", file = "./assets/fonts/charriot.ttf", size = 14}
	},

	map = {
		textureAssetId = "TileMap",
		file = "./assets/tilemaps/jungle.map",
		scale = 2,
		tileSize = 32,
		mapSizeX = 25,
		mapSizeY = 20
	},

	entities = {
		[0] = {
			name = "player",
			layer = 4,

			components = {
				transform = {
					position = {
						x = 240,
						y = 106
					},
					velocity = {
						x = 0,
						y = 0
					},
					width = 32,
					height = 32,
					scale = 1,
					rotation = 0
				},
				sprite  = {
					textureAssetId = "ChopperSpriteSheet",
					animated = true,
					frameCount = 2,
					animationSpeed = 90, 
					hasDirections = true,
					fixed = false
				},

				collider = {
					tag = "PLAYER"
				},
				input = {
					keyboard = {
						up = "w",
						left = "a",
						down = "s",
						right = "d",
						shoot = "space"
					}
				}
			}

		}
	}
}