#pragma once
#include "CreateImguiContext.h"


class ImguiTestWindow
{
private:
	GLFWwindow* window;
	bool err;
    bool show_demo_window = true;
    bool show_another_window = false;
	ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

public:
	void SetWindowContext(GLFWwindow* window);
	void ClearData();
	void UpdateWindow();
	int CheckIfWindowShouldClose() const;
};

