#pragma once
#include "ImguiDefinitions.h"
#include "../Constants/Constants.h"

class CreateImguiContext
{
private:
	ImVec4 clear_color;
	bool err;
public:
	GLFWwindow* CreateImguiContextWindowFunc(); 
};

