#include "ShowScrollableTextUI.h"

void ShowScrollableTextUI::SetWindowContext(GLFWwindow* window)
{
	this->window = window;
}

void ShowScrollableTextUI::SetTextToPrintOut(std::vector<std::string> textToPrintOut)
{
	this->textToPrintOut = textToPrintOut;
}

void ShowScrollableTextUI::SetWindowName(std::string name)
{
	this->windowName = name;
}

void ShowScrollableTextUI::ClearData()
{
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    glfwDestroyWindow(window);
    glfwTerminate();
}

void ShowScrollableTextUI::UpdateWindow()
{	
	glfwPollEvents();

	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();

	auto tempWindowName = &windowName[0];

	ImGui::Begin(tempWindowName);                          // Create a window called "Hello, world!" and append into it.


	for (auto txt : textToPrintOut) {
		char* tempChar;

		tempChar = &txt[0];

		ImGui::Text(tempChar);
	}

    ImGui::End();


	ImGui::Render();
	int display_w, display_h;
	glfwGetFramebufferSize(window, &display_w, &display_h);
	glViewport(0, 0, display_w, display_h);
	glClearColor(clear_color.x * clear_color.w, clear_color.y * clear_color.w, clear_color.z * clear_color.w, clear_color.w);
	glClear(GL_COLOR_BUFFER_BIT);
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

	glfwSwapBuffers(window);

}

int ShowScrollableTextUI::CheckIfWindowShouldClose() const
{
    return glfwWindowShouldClose(window);
}
