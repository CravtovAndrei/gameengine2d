#pragma once
#include "CreateImguiContext.h"
#include <vector>
#include <string>

class ShowScrollableTextUI
{
	private:
		GLFWwindow* window;
		ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
		std::vector<std::string> textToPrintOut;
		std::string windowName = "Default Window";

	public:
		void SetWindowContext(GLFWwindow* window);
		void SetTextToPrintOut(std::vector<std::string> textToPrintOut);
		void SetWindowName(std::string name);
		void ClearData();
		void UpdateWindow();
		int CheckIfWindowShouldClose() const;
};

