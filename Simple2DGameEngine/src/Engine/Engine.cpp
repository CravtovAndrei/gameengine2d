#include <iostream>
#include "Engine.h"
#include "../EntityComponentSystem/EntityManager.h"
#include "../Engine/MapManager/Map.h"
#include "../Components/TransformComponent.h"
#include "../Components/ProjectileEmitterComponent.h"
#include "../Components/SpriteComponent.h"
#include "../Components/KeyboardControlcomponent.h"
#include "../../libs/glm/glm.hpp"
#include "../Components/Colliders/ColliderComponent.h"
#include "../Components/Text/TextLabelComponent.h"

#include "al.h" 
#include "alc.h" 

EntityManager entityManager;
Map* map;
AssetsManager* Engine::assetsManager = new AssetsManager(&entityManager);
SDL_Renderer* Engine::renderer;
SDL_Event Engine::event;

SDL_Rect Engine::camera = { 0,0, WINDOW_WIDTH, WINDOW_HEIGHT };


Engine::Engine()
{
	this->isRunning = false;
}

void Engine::InitializeEngine(int width, int height)
{
	TimeTick = new Time();

	if (!TimeTick) {
		std::cerr << "Error with creating TimeTick object." << std::endl;
		return;
	}

	if (TTF_Init() != 0) {
		std::cerr << "Error initializing SDL TTF" << std::endl;
	}

	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
		std::cerr << "Error with initializing SDL Libraries." << std::endl;
		return;
	}

	window = SDL_CreateWindow(NULL, 
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 
		width, height, SDL_WINDOW_BORDERLESS);
	if (!window) {
		std::cerr << "Error creating SDL Window." << std::endl;
		return;
	}

	renderer = SDL_CreateRenderer(window, -1, 0);
	if (!renderer) {
		std::cerr << "Error creating SDL Renderer." << std::endl;
		return;
	}

	LoadLevel(1);

	isRunning = true;
	return;
}


void Engine::LoadLevel(int levelNumber)
{
	sol::state lua; 
	lua.open_libraries(sol::lib::base, sol::lib::os, sol::lib::math);

	std::string levelName = "Level" + std::to_string(levelNumber);
	lua.script_file("./src/LUAScripts/" + levelName + ".lua");
	sol::table levelData = lua[levelName];


	//Load Assets
	sol::table levelAssets = levelData["assets"];

	unsigned int assetIndex = 0;

	while (true) {
		sol::optional<sol::table> existsAssetIndexNode = levelAssets[assetIndex];

		if (existsAssetIndexNode == sol::nullopt) {
			break;
		}
		else {
			sol::table asset = levelAssets[assetIndex];
			std::string assettype = asset["type"];

			std::string assetid = asset["id"];
			std::string assetfile = asset["file"];

			if (assettype.compare("texture") == 0) {
				assetsManager->AddTexture(assetid, assetfile.c_str());
			}

			if (assettype.compare("font") == 0) {
				
				int fontSize = static_cast<int>(asset["size"]);
				assetsManager->AddFont(assetid, assetfile.c_str(), fontSize);
			}
		}
		assetIndex++;
	}

	////Load Map

	sol::table levelMap = levelData["map"];
	std::string mapTextureId = levelMap["textureAssetId"];
	std::string mapFile = levelMap["file"];

	map = new Map(mapTextureId, static_cast<int>(levelMap["scale"]),static_cast<int>(levelMap["tileSize"]));
	map->LoadMap(levelMap["file"], static_cast<int>(levelMap["mapSizeX"]), static_cast<int>(levelMap["mapSizeY"]));

	//// LOAD Entities

	sol::table levelEntities = levelData["entities"];


	unsigned int entityIndex = 0; 

	while (true) {
		sol::optional<sol::table> existsEntityIndexNode = levelEntities[entityIndex];

		if (existsEntityIndexNode == sol::nullopt) {
			break;
		}
		else {

			sol::table currentLuaEntity = levelEntities[entityIndex];
			sol::table currentLuaEntityComponents = currentLuaEntity["components"]; //todo check all components and see if it has transform

		
			Entity& loadedEntity(entityManager.AddEntity(currentLuaEntity["name"], currentLuaEntity["layer"]));

			sol::optional<sol::table> checkIfTransformExists = currentLuaEntityComponents["transform"]; //todo refactor
			sol::optional<sol::table> checkIfSpriteExists = currentLuaEntityComponents["sprite"]; //todo refactor
			sol::optional<sol::table> checkIfColliderExists = currentLuaEntityComponents["collider"]; //todo refactor
			sol::optional<sol::table> checkIfInputExists = currentLuaEntityComponents["input"]; //todo refactor

			if (checkIfTransformExists == sol::nullopt) {std::cout << "No Transform Component is Attached" << std::endl;}
			else 
			{

				sol::table currentPosition = currentLuaEntityComponents["transform"]["position"];
				sol::table currentVelocity = currentLuaEntityComponents["transform"]["velocity"];
				int width = currentLuaEntityComponents["transform"]["width"];
				int height = currentLuaEntityComponents["transform"]["height"];
				int scale = currentLuaEntityComponents["transform"]["scale"];
				int rotation = currentLuaEntityComponents["transform"]["rotation"];
				int xPos = currentPosition["x"];
				int yPos = currentPosition["y"];

				loadedEntity.AddComponent<TransformComponent>(xPos, yPos, 
															  currentVelocity["x"], currentVelocity["y"], 
															  width, height, scale);

				if(checkIfColliderExists == sol::nullopt) {std::cout << "No Collider Component is Attached" << std::endl;}
				else 
				{
					sol::table colliderTable = currentLuaEntityComponents["collider"];
					std::string tag = colliderTable["tag"];

					loadedEntity.AddComponent<ColliderComponent>(tag, xPos, yPos, 32, 32);
				}
			}


			if (checkIfSpriteExists == sol::nullopt) {std::cout << "No Sprite Component is Attached" << std::endl;}
			else 
			{
				sol::table spriteTable = currentLuaEntityComponents["sprite"];
				std::string textureAssetId = spriteTable["textureAssetId"];

				bool animated = spriteTable["animated"];
				int frameCount = spriteTable["frameCount"];
				int animationSpeed = spriteTable["animationSpeed"];
				bool hasDirections = spriteTable["hasDirections"];
				bool fixed = spriteTable["fixed"];
				

				if(animated)
					loadedEntity.AddComponent<SpriteComponent>(textureAssetId.c_str(), frameCount, animationSpeed, hasDirections, fixed);
				else
					loadedEntity.AddComponent<SpriteComponent>(textureAssetId.c_str());
			}



			//loadedEntity.AddComponent<ColliderComponent>("Projectile", 0, 0, 32, 32);
			//loadedEntity.AddComponent<ProjectileEmitterComponent>(50, 0, 200, true);
			//loadedEntity.AddComponent<KeyboardControlComponent>("up","right","down","left","space");


	//Entity& projectile(entityManager.AddEntity("Projectile", PROJECTILE_LAYER));
	//projectile.AddComponent<TransformComponent>(0, 0, 0, 0, 4, 4, 1);
	//projectile.AddComponent<SpriteComponent>("Bullet");
	//projectile.AddComponent<ColliderComponent>("PROJECTILE", 0,0,4,4);
	//projectile.AddComponent<ProjectileEmitterComponent>(50, 0, 200, true);

		}

		entityIndex++;
	}


	//assetsManager->AddTexture("tile-map", std::string("./assets/tilemaps/jungle.png").c_str());
//	map = new Map(mapTextureId, 2, 32);
//	map->LoadMap("./assets/tilemaps/jungle.map", 25,20);

	////Load Assets
	//std::string textureFilePath = "./assets/images/bullet.png";
	//std::string animatedTextureFilePath = "./assets/images/chopper-spritesheet.png";

	//assetsManager->AddTexture("Bullet", textureFilePath.c_str());
	//assetsManager->AddTexture("ChopperSpritesheet", animatedTextureFilePath.c_str());
	//assetsManager->AddFont("charriot-font", std::string("./assets/fonts/charriot.ttf").c_str(), 14);


	////New Entities and Components
	//Entity& chopper(entityManager.AddEntity("Chopper", PROJECTILE_LAYER));
	//chopper.AddComponent<TransformComponent>(100,200, 0, 0, 32,32, 1);
	//chopper.AddComponent<SpriteComponent>("ChopperSpritesheet", 2, 90, true, false);
	//chopper.AddComponent<KeyboardControlComponent>("up","right","down","left","space");

	//Entity& enemy(entityManager.AddEntity("enemy", PROJECTILE_LAYER)); 
	//enemy.AddComponent<TransformComponent>(0,0, 20, 20, 32,32, 1);
	//enemy.AddComponent<SpriteComponent>("TankImage");
	//enemy.AddComponent<ColliderComponent>("enemy", 0,0,32,32);

	//Entity& projectile(entityManager.AddEntity("Projectile", PROJECTILE_LAYER));
	//projectile.AddComponent<TransformComponent>(0, 0, 0, 0, 4, 4, 1);
	//projectile.AddComponent<SpriteComponent>("Bullet");
	//projectile.AddComponent<ColliderComponent>("PROJECTILE", 0,0,4,4);
	//projectile.AddComponent<ProjectileEmitterComponent>(50, 0, 200, true);



	//Entity& labelLevelName(entityManager.AddEntity("LavelLevelName", UI_LAYER));
	//labelLevelName.AddComponent<TextLabelComponent>(10,10, "Level 1", "charriot-font", WHITE_COLOR_SDL);
}


bool Engine::IsRunning() const
{
	return this->isRunning;
}

void Engine::ProcessInput()
{
	SDL_PollEvent(&event);

	switch (event.type)
	{
		case SDL_QUIT: {
			isRunning = false;
			break;
		}
		case SDL_KEYDOWN: {
			if (event.key.keysym.sym == SDLK_ESCAPE) {
				isRunning = false;
				break;
			}
		}
		default:
			break;
	}

}

void Engine::Update()
{
	TimeTick->TimeUpdate();

	entityManager.Update(Time::GetDeltaTime());

	HandleCameraMovement();

	CheckCollisions();
}

void Engine::Render()
{
	SDL_SetRenderDrawColor(renderer, 21, 21, 21, 255);
	SDL_RenderClear(renderer);

	if (entityManager.HasNoEntities()) return;

	entityManager.Render();

	SDL_RenderPresent(renderer);
}

void Engine::HandleCameraMovement()
{
	//TransformComponent* mainPlayerTransform = playerEntity.GetComponent<TransformComponent>();

	//camera.x = mainPlayerTransform->position.x - (WINDOW_WIDTH / 2);
	//camera.y = mainPlayerTransform->position.y - (WINDOW_HEIGHT / 2);

	//camera.x = camera.x < 0 ? 0 : camera.x;
	//camera.y = camera.y < 0 ? 0 : camera.y;

	//camera.x = camera.x > camera.w ? camera.w : camera.x;
	//camera.y = camera.y > camera.h ? camera.h : camera.y;
}

void Engine::CheckCollisions() {
	CollisionType collisionType = entityManager.CheckEntityCollisions();

	if (collisionType == PLAYER_PROJECTILE_COLLISION) {
		std::cout << "Projectile Collision" << std::endl;
	}
}

void Engine::Destroy()
{
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
}

Engine::~Engine()
{
}

std::vector<std::string> Engine::ReturnAllEntitiesNames() const
{
	return entityManager.ReturnAllEntitiesNames();
}
