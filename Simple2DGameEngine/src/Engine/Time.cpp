#include "Time.h"
#include <iostream>
#include "../Constants/Constants.h"

float* Time::deltaTime;

Time::Time()
{
	deltaTime = &deltaTimeMemoryAdress;
}

void Time::TimeUpdate()
{
	int timeToWait = FRAME_TARGET_TIME - (SDL_GetTicks() - ticksLastFrame);

	if (timeToWait > 0 && timeToWait <= FRAME_TARGET_TIME) SDL_Delay(timeToWait);

	*deltaTime = (SDL_GetTicks() - ticksLastFrame) / 1000.0f; //todo remove to TIME const class later

	*deltaTime = (*deltaTime > 0.05f) ? 0.05f : *deltaTime;

	ticksLastFrame = SDL_GetTicks();
}

float Time::GetDeltaTime() 
{
	return *deltaTime;
}

