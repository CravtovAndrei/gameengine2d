#pragma once
#include "../../EntityComponentSystem/EntityManager.h"
#include "../AssetsManager/TextureManager.h"
#include "../AssetsManager/FontManager.h"
#include <map>
#include <string>

class AssetsManager
{
	private:
		EntityManager* manager;
		std::map<std::string, SDL_Texture*> textures;
		std::map<std::string, TTF_Font*> fonts;

	public:
		AssetsManager(EntityManager* manager);
	    ~AssetsManager();
		void ClearData();
		void AddTexture(std::string textureId, const char* filePath);
		void AddFont(std::string fontId, const char* filePath, int fontSize);

		SDL_Texture* GetTexture(std::string textureId);
		TTF_Font* GetFont(std::string fontId);

};

