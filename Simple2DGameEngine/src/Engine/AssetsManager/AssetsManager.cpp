#include "AssetsManager.h"

AssetsManager::AssetsManager(EntityManager* manager) : manager(manager)
{
}

void AssetsManager::ClearData()
{
	textures.clear();
	fonts.clear();
}

void AssetsManager::AddTexture(std::string textureId, const char* filePath)
{
	textures.emplace(textureId, TextureManager::LoadTexture(filePath));
}


SDL_Texture* AssetsManager::GetTexture(std::string textureId)
{
	return textures[textureId];
}

void AssetsManager::AddFont(std::string fontId, const char* filePath, int fontSize)
{
	fonts.emplace(fontId, FontManager::LoadFont(filePath, fontSize));
}

TTF_Font* AssetsManager::GetFont(std::string fontId)
{
	return fonts[fontId];
}

AssetsManager::~AssetsManager()
{
}
