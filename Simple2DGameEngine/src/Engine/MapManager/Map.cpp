#include <fstream>
#include "Map.h"
#include "../Engine.h"
#include "../../EntityComponentSystem/EntityManager.h"
#include "../../Components/MapComponents/TileComponent.h"

extern EntityManager entityManager;

Map::Map(std::string textureId, int scale, int tileSize)
{
	this->textureId = textureId;
	this->scale = scale;
	this->tileSize = tileSize;
}

void Map::LoadMap(std::string filePath, int mapSizeX, int mapSizeY)
{
	std::fstream mapFile;
	mapFile.open(filePath);

	for (int y = 0; y < mapSizeY; y++) 
	{
		for (int x = 0; x < mapSizeX; x++) 
		{
			char ch;
			mapFile.get(ch);
			int sourceRectY = atoi(&ch) * tileSize;
			mapFile.get(ch);

			int sourceRectX = atoi(&ch) * tileSize;
			float posMultiplier = scale * tileSize;

			AddTile(sourceRectX, sourceRectY, x * posMultiplier, y * posMultiplier);
			mapFile.ignore();
		}
	}
	mapFile.close();
}

void Map::AddTile(int sourceX, int sourceY, int posX, int posY)
{
	Entity& newTile(entityManager.AddEntity("Tile", TILEMAP_LAYER));
	newTile.AddComponent<TileComponent>(sourceX, sourceY, posX, posY, tileSize, scale, textureId);
}

Map::~Map()
{
}
