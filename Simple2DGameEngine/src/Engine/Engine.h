#pragma once
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h> //todo perhaps move these sdls somewhere else
#include "../sol/sol.hpp"
#include "../Engine/Time.h"
#include "../EntityComponentSystem/Entity.h"
#include "AssetsManager/AssetsManager.h"
#include "../Constants/TypesConstants.h"


class AssetsManager;
class Engine {

private:
	bool isRunning = false;
	SDL_Window* window = nullptr;
	Time* TimeTick = nullptr;
	
public:
	Engine();
	static AssetsManager* assetsManager;
	static SDL_Renderer* renderer;
	static SDL_Event event;
	static SDL_Rect camera;
	void HandleCameraMovement();
	void LoadLevel(int levelNumber);
	void InitializeEngine(int width, int height);
	bool IsRunning() const;
	void ProcessInput();
	void Update();
	void Render();
	std::vector<std::string> ReturnAllEntitiesNames() const;
	void CheckCollisions();


	void Destroy();
	~Engine();
};

