#pragma once
#ifndef TIME_H 
#define TIME_H
#include <SDL.h>

class Time {
private: 
	int timeToWait = 0;
	int ticksLastFrame = 0;
	float deltaTimeMemoryAdress;
	static float* deltaTime;

public:
	Time();
	void TimeUpdate();
	static float GetDeltaTime();
};

#endif
