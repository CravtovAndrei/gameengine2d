#pragma once
#include <string>

const std::string transformType = "TransformComponent";
const std::string spriteType = "SpriteComponent";
const std::string projectileType = "ProjectileEmitterComponent";
const std::string keyboardControlType = "KeyboardControlComponent";
const std::string textLabelType = "TextLabelComponent";
const std::string mapTyleType = "TyleComponent";
const std::string colliderType = "ColliderComponent";

