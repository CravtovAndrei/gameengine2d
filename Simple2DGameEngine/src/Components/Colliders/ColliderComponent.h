#pragma once
#include "../../EntityComponentSystem/EntityManager.h"
#include "../../Engine/Engine.h"
#include "../TransformComponent.h"

class ColliderComponent: public Component
{
	public:
		std::string colliderTag; //perhaps must be a enum
		SDL_Rect collider;
		SDL_Rect sourceRectangle;
		SDL_Rect destinationRectangle;
		TransformComponent* transform;

		ColliderComponent(std::string colliderTag, int x, int y, int width, int height) 
		{
			this->colliderTag = colliderTag;
			this->collider = { x,y,width,height };
		}

		ColliderComponent(std::string colliderTag, int x, int y, int width, int height, bool drawColliderOnTop) 
		{
			this->colliderTag = colliderTag;
			this->collider = { x,y,width,height };
			this->drawColliderOnTop = drawColliderOnTop;
		}

		void Initialize() override {
			if (owner->HasComponent<TransformComponent>()) {
				transform = owner->GetComponent<TransformComponent>();
				sourceRectangle = { 0,0, transform->width, transform->height };
				destinationRectangle = { collider.x, collider.y, collider.w, collider.h };
			}
		}

		void Update(float deltaTime) override 
		{
			collider.x = static_cast<int>(transform->position.x);
			collider.y = static_cast<int>(transform->position.y);
			collider.w = transform->width * transform->scale;
			collider.h = transform->height * transform->scale;
			destinationRectangle.x = collider.x - Engine::camera.x;
			destinationRectangle.y = collider.y - Engine::camera.y;

		}

		void Render() override 
		{
			if (!drawColliderOnTop) return;

			SDL_SetRenderDrawColor(Engine::renderer, 0, 255, 0, 255);
			SDL_RenderFillRect(Engine::renderer, &destinationRectangle);
			SDL_RenderPresent(Engine::renderer);
		}

	private:
		bool drawColliderOnTop = false;
};

