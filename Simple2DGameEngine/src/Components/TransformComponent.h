#pragma once

#include "../EntityComponentSystem/EntityManager.h"
#include "../../libs/glm/glm.hpp"
#include <SDL_rect.h>
#include <SDL.h>
#include "../Engine/Engine.h"
#include "../Constants/Constants.h"


class TransformComponent : public Component {
public:
	glm::vec2 position;
	glm::vec2 velocity;
	int width;
	int height;
	int scale; //todo perhaps add here a vec2

	TransformComponent() 
	{
		position = glm::vec2(0, 0);
		velocity = glm::vec2(0, 0);
		width = 1;
		height = 1;
		scale = 1;
	}

	TransformComponent(int posX, int posY, int velX, int velY, int w, int h, int s) {
		position = glm::vec2(posX, posY);
		velocity = glm::vec2(velX, velY);
		width = w;
		height = h;
		scale = s;
	}

	void Initialize() override;
	void Update(float deltaTime) override {
		position.x += velocity.x * deltaTime;
		position.y += velocity.y * deltaTime;

		ClampPositionToScreenBorder();
		
	}

	void ClampPositionToScreenBorder()
	{
		position.x = glm::clamp(position.x, 0.0F, (float)WINDOW_WIDTH - width);
		position.y = glm::clamp(position.y, 0.0F, (float)WINDOW_HEIGHT - height);
	}


	void Render() override {
		//SDL_Rect transformRectangle = { //maybe remove this and make some Gizmos components
		//	(int) position.x, (int) position.y, width, height
		//};
		//
		//SDL_SetRenderDrawColor(Engine::renderer, 255, 255, 255, 255);
		//SDL_RenderFillRect(Engine::renderer, &transformRectangle);
	}
};
