#pragma once

#include <SDL.h>
#include "../EntityComponentSystem/EntityManager.h"
#include "../Engine/AssetsManager/TextureManager.h"
#include "../Engine/AssetsManager/AssetsManager.h"
#include "TransformComponent.h"
#include "Animation/Animation.h"

class SpriteComponent : public Component
{
	private:
		TransformComponent* transform;
		SDL_Texture* texture;
		SDL_Rect sourceRectangle;
		SDL_Rect destinationRectangle;
		bool isAnimated;
		bool isFixed;
		int numberOfFrames;
		int animationSpeed;
		std::map<std::string, Animation> animations;
		std::string currentAnimationName;
		unsigned int animationIndex = 0;


	public:
		SDL_RendererFlip spriteFlip = SDL_FLIP_NONE;

		SpriteComponent(const char* assetTextureID) {
			isAnimated = false;
			isFixed = false;
			SetTexture(assetTextureID);
		}

		SpriteComponent(const char* assetTextureID, int numberOfFrames, int animationSpeed, bool hasDirections, bool isFixed) {
			isAnimated = true;
			this->numberOfFrames = numberOfFrames;
			this->animationSpeed = animationSpeed;
			this->isFixed = isFixed;

			if(hasDirections) 
			{
				Animation up = Animation(0, numberOfFrames, animationSpeed);
				Animation right = Animation(1, numberOfFrames, animationSpeed);
				Animation down = Animation(2, numberOfFrames, animationSpeed);
				Animation left = Animation(3, numberOfFrames, animationSpeed);


				animations.emplace("DownAnimation", down);
				animations.emplace("RightAnimation", right);
				animations.emplace("LeftAnimation", left);
				animations.emplace("UpAnimation", up);

				this->animationIndex = 0;
				this->currentAnimationName = "DownAnimation";
			}
			else 
			{
				Animation singleAnimation = Animation(0, numberOfFrames, animationSpeed);
				std::string animationName = "SingleAnimation";
				animations.emplace(animationName, singleAnimation);
				this->animationIndex = 0;
				this->currentAnimationName = animationName;
			}
			Play(this->currentAnimationName);
			SetTexture(assetTextureID);
		}

		void SetTexture(std::string assetTextureID) {
			texture = Engine::assetsManager->GetTexture(assetTextureID);
		}

		void Play(std::string currentAnimationName) 
		{
			numberOfFrames = animations[currentAnimationName].numberOfFrames;
			animationIndex = animations[currentAnimationName].index;
			animationSpeed = animations[currentAnimationName].animationSpeed;
			this->currentAnimationName = currentAnimationName;
		}

		void Initialize() override {
			transform = owner->GetComponent<TransformComponent>();
			sourceRectangle.x = 0;
			sourceRectangle.y = 0;
			sourceRectangle.w = transform->width;
			sourceRectangle.h = transform->height;
		}

		void Update(float deltaTime) override {
			if (isAnimated) 
			{
				sourceRectangle.x = sourceRectangle.w * static_cast<int>((SDL_GetTicks() / animationSpeed) % numberOfFrames);
				sourceRectangle.y = animationIndex * transform->height;
			}

			destinationRectangle.x = static_cast<int>(transform->position.x) - (isFixed ? 0 : Engine::camera.x);
			destinationRectangle.y = static_cast<int>(transform->position.y) - (isFixed ? 0 : Engine::camera.y);
			destinationRectangle.w = transform->width * transform->scale;
			destinationRectangle.h = transform->height * transform->scale;
		}
		void Render() override {
			TextureManager::DrawTexture(texture, sourceRectangle, destinationRectangle, spriteFlip);
		}
};

