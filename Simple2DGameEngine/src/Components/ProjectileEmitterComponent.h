#pragma once

#include "../../libs/glm/glm.hpp"
#include "../EntityComponentSystem/EntityManager.h"
#include "../Components/TransformComponent.h"

class ProjectileEmitterComponent : public Component
{
private:
    TransformComponent* transform;
    glm::vec2 origin;
    int speed;
    int range;
    float angleRadians; 
    bool IsLooping;

public:

    ProjectileEmitterComponent(int speed, int angleDegrees, int range, bool IsLooping);

    void Initialize() override{
        transform = owner->GetComponent<TransformComponent>();
        origin = glm::vec2(transform->position.x, transform->position.y);
        transform->velocity = glm::vec2(glm::cos(angleRadians) * speed, glm::sin(angleRadians) * speed);
    }

    void Update(float deltaTime) override{
        if(glm::distance(transform->position, origin) > range)
        {
            if(IsLooping)
            {
                transform->position.x = origin.x;
                transform->position.y = origin.y;
            }
            else
            {
               owner->Destroy() ;
            }
        }
    }

    ~ProjectileEmitterComponent();
};
