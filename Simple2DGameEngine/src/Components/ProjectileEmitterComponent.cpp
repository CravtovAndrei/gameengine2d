#include "ProjectileEmitterComponent.h"


ProjectileEmitterComponent::ProjectileEmitterComponent(int speed, int angleDegrees, int range, bool IsLooping){
	this->speed = speed;
	this->range = range;
	this->IsLooping = IsLooping;
	this->angleRadians = glm::radians(static_cast<float>(angleDegrees));
}

ProjectileEmitterComponent::~ProjectileEmitterComponent()
{
}
