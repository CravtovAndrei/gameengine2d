#pragma once
#include <SDL.h>
#include <SDL_ttf.h>
#include "../../Engine/AssetsManager/FontManager.h"
#include "../../EntityComponentSystem/EntityManager.h"
#include "../../Engine/AssetsManager/AssetsManager.h"
#include "../../Engine/Engine.h"

class TextLabelComponent : public Component
{
private:
	SDL_Rect position;
	std::string textContent;
	std::string fontFamily;
	SDL_Color textColor;
	SDL_Texture* textTexture;
public:
	TextLabelComponent(int x, int y, std::string textContent, std::string fontFamily, const SDL_Color textColor) 
	{
		this->position.x = x;
		this->position.y = y;
		this->textContent = textContent;
		this->fontFamily = fontFamily;
		this->textColor = textColor;

		SetLabelText(textContent, fontFamily);
	}

	void SetLabelText(std::string text, std::string fontFamily) {
		
		SDL_Surface* surface = TTF_RenderText_Blended(Engine::assetsManager->GetFont(fontFamily), text.c_str(), textColor);
		textTexture = SDL_CreateTextureFromSurface(Engine::renderer, surface);
		SDL_FreeSurface(surface);
		SDL_QueryTexture(textTexture, NULL, NULL, &position.w, &position.h);
	}

	void Render() override {
		FontManager::Draw(textTexture, position);
	}
};

