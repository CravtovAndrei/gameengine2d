#pragma once

#include <SDL.h>
#include "../glm/glm.hpp"
#include "../../Engine/AssetsManager/AssetsManager.h"
#include "../../EntityComponentSystem/EntityManager.h"

class TileComponent : public Component
{
public: 
	SDL_Texture* texture;
	SDL_Rect sourceRectangle;
	SDL_Rect destinationRectangle;
	glm::vec2 position;

	TileComponent(int sourceRectX, int sourceRectY, int posX, int posY, int tileSize, int tileScale, std::string assetTextureId) {
		texture = Engine::assetsManager->GetTexture(assetTextureId);

		sourceRectangle.x = sourceRectX;
		sourceRectangle.y = sourceRectY;
		sourceRectangle.w = tileSize;
		sourceRectangle.h = tileSize;

		destinationRectangle.x = posX;
		destinationRectangle.y = posY;
		destinationRectangle.w = tileSize * tileScale;
		destinationRectangle.h = tileSize * tileScale;

		position.x = posX;
		position.y = posY;
	};

	~TileComponent() {
	
		SDL_DestroyTexture(texture);
	
	};

	void Update(float deltaTime) override {
		destinationRectangle.x = position.x - Engine::camera.x;
		destinationRectangle.y = position.y - Engine::camera.y;
	}

	void Render() override {
		TextureManager::DrawTexture(texture, sourceRectangle, destinationRectangle, SDL_FLIP_NONE);
	};
};

