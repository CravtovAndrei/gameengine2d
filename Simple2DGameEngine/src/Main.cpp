#pragma comment(lib, "legacy_stdio_definitions.lib") //this thing is needed for glfw to work 
#include <iostream>
#include "./Engine/Engine.h"
#include "./Constants/Constants.h"
#include "UI/ShowScrollableTextUI.h"
#include "UI/CreateImguiContext.h"

int main(int argc, char* argv[]) {

    Engine engine;
    engine.InitializeEngine(WINDOW_WIDTH, WINDOW_HEIGHT);

    ShowScrollableTextUI scrollInfo = ShowScrollableTextUI();
    scrollInfo.SetTextToPrintOut(engine.ReturnAllEntitiesNames());

    CreateImguiContext imguiContextWindow = CreateImguiContext();
    GLFWwindow* window = imguiContextWindow.CreateImguiContextWindowFunc();

    scrollInfo.SetWindowContext(window);

    while (engine.IsRunning() && !scrollInfo.CheckIfWindowShouldClose())
    {
        scrollInfo.UpdateWindow();
        engine.ProcessInput();
        engine.Update();
        engine.Render();
    }

    scrollInfo.ClearData();
    engine.Destroy();

    return 0;
}
